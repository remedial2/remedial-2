# nflapp/admin.py
from django.contrib import admin
from .models import Owner, Stadium, Player, Team

@admin.register(Owner)
class OwnerAdmin(admin.ModelAdmin):
    list_display = ('name',)

@admin.register(Stadium)
class StadiumAdmin(admin.ModelAdmin):
    list_display = ('name', 'venue', 'capacity', 'box_size')

@admin.register(Player)
class PlayerAdmin(admin.ModelAdmin):
    list_display = ('name', 'number', 'position', 'team')

@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = ('name', 'owner', 'stadium')