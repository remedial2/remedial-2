# nflapp/models.py
from django.db import models

class Owner(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class Stadium(models.Model):
    name = models.CharField(max_length=255)
    venue = models.CharField(max_length=255)
    capacity = models.IntegerField()
    box_size = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class Player(models.Model):
    name = models.CharField(max_length=255)
    number = models.IntegerField()
    position = models.CharField(max_length=255)
    team = models.ForeignKey('Team', on_delete=models.CASCADE)

    def __str__(self):
        return self.name

class Team(models.Model):
    name = models.CharField(max_length=255)
    owner = models.ForeignKey(Owner, on_delete=models.CASCADE)
    stadium = models.ForeignKey(Stadium, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
