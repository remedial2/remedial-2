
from django.shortcuts import render, get_object_or_404
from .models import Equipo

def lista_equipos(request):
    equipos = Equipo.objects.all()
    return render(request, 'lista_equipos.html', {'equipos': equipos})

def detalle_equipo(request, equipo_id):
    equipo = get_object_or_404(Equipo, pk=equipo_id)
    return render(request, 'detalle_equipo.html', {'equipo': equipo})
